import requests
from bs4 import BeautifulSoup as bs
import csv
from pprint import pprint

URL = "https://www.imdb.com/chart/top/"
page = requests.get(URL)
html = page.content
soup = bs(html, "html.parser")

data = {'rank' : [], 'title': [], 'rating' : []}
movies = soup.find_all("td", class_="titleColumn")
ratings = soup.find_all("td", class_="imdbRating")

for rating in ratings:
    rating_text = rating.get_text(strip=True)
    data["rating"].append(rating_text)
    
for rank in movies:
    movie_text = rank.get_text(strip=True)
    
    rank_text = movie_text.split('.')[0]
    
    title_text = movie_text.split('.')[1]
    
    data["rank"].append(rank_text)
    data["title"].append(title_text)

movies_list = [];

for x in range(len(data["rank"])):
        current_rank = data["rank"][x]
        current_title = data["title"][x]
        current_rating = data["rating"][x]
        
        movies_list.append([current_rank,current_title,current_rating])
        
''' pprint(movies_list) '''
    
import csv

header = ['Rank', 'Title', 'Rating']
with open('movies.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(header)
    
    for movie in movies_list:
        writer.writerow(movie)

